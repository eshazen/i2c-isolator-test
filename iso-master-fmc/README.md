# I2C isolated master

This is a 6-channel I2C master with electrically isolated buses.
It is intended to fit in the D-DOSI system spare FMC site to control
up to 6 laser boards via I2C.

It includes an ATMegaxx8 MCU and a Microchip PCA9547 I2C bus switch along
with an ISO1642 I2C bus isolator.

Note that the bus isolator supports only unidirectional SCL from the controller
(master) to target (slave).  This shouldn't be a problem for the intended
application.

## Issues with Rev 1.1

* The level shifter (Q1) is still wired wrong (sigh!).  The net names TX50 and RX50
on the schematic are swapped.  Fix below works.

## Issues with Rev 1

* Q1 is wrong pkg.  Got clever with a dual-MOSFET level shifter but used wrong footprint.
The package should be SO-363, not SOT23-6.  Will need to do some bricolage on the first boards.
* MOSI and MISO swapped on ISP connector
* R4 and R5 shuld be smaller (red LEDs are quite dim)
* Missing pull-ups on all local I2C buses (sigh!)
* Missing pull-ups on all remote I2C buses (double sigh!)
* ISP connector should be unshrouded header
* TX and RX swapped on J4 (vs FTDI cable)

## Debug/test log

Rev 1.1 board programs and communicates over serial FTDI cable and ISP pinouts now correct :)
Working on fix for level shifters.  Tracking again the pinout.  It's H4, H5 on the FMC.

    FMC Carrier                                 PX C04
    H4  CLK0M2C_P  S6_CLKOUT_P  CLK_P5  JX3-73  BANK13_LVDS_7_P  Y7  FPGA RX
	H5  CLK0M2C_N  S6_CLKOUT_N  CLK_N5  JX3-75  BANK13_LVDS_7_N  Y6  FPGA TX
	
* Jumper J2 5-6 to connect RX path from RX25 through shifter to RX50
* Cut trace on FMC side from FMC-H4 to R9 near via
* Wire J2-4  to cut trace from FMC-H4
* Wire J2-3 (MCU TX) to TX25 (R9 bottom)

This works; the UART communicates fine with the Zynq.

--------------------

Rev 1 board is working fine on two laser drivers (with I2C pull-ups added).

There is still an occasional I2C bus error condition after power-up or
replugging the mini-din cables.

Need to test the level shifter serial port.  Thinking to simplify the
circuit instead of the clever bidirectional one:  Tx: 10k resistor
divider.  Rx: MOSFET switch for 2.5V to 5V.

Checkout 'develop' to start on this on botlab.

## Power estimate

Total 5V estimate

| Qty | Device                      | Each   | Total  |
|-----|-----------------------------|:-------|:-------|
| 1   | ATMega88 (8MHz@5V)          | 0.005  | 0.005  |
| 1   | PCA9547                     | 0.0001 | 0.0001 |
| 12  | I2C pull-ups                | 0.001  | 0.012  |
| 6   | ISO1642 (side 1 worst case) | 0.010  | 0.060  |
| 5   | LED                         | 0.002  | 0.010  |
|     |                             |        | 0.087  |

A linear regulator from 12V-5V would dissipate 600mW.
Probably should stay with the TO-220 and put a heatsink on it.
