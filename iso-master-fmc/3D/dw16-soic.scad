//
// ISO1642 DW16 package
// SOIC body 7.6 x 10.5

bod_x = 7.5;
bod_y = 10.3;
bod_z = 2.65;

pin_x = 1.4;
pin_y = 0.5;
pin_z = 0.25;

module bod() {
  color("black")
    translate( [-bod_x/2, -bod_y/2, 0])
    cube( [bod_x, bod_y, bod_z]);
}

pin_span = 1.27*7;

module dw16() {
  bod();
  for( y=[0:1.27:pin_span]) {
    translate( [-bod_x/2-pin_x, y-4.7, 0])
      color("grey")
      cube( [pin_x, pin_y, pin_z]);
    translate( [bod_x/2, y-4.7, 0])
      color("grey")
      cube( [pin_x, pin_y, pin_z]);
  }
}


dw16();

