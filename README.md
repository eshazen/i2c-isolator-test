# I2C Isolator board

This is an I2C bus master with multiple, electrically-isolated buses and
a microcontroller with UART interface.  This board is intended to mount
in an FMC site in the D-DOSI system but could be used for other purposes.

`iso-master-fmc` has the design files.

`iso-test-master` is a now abandoned test design.


